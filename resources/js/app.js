/**
 * Next, we will create a fresh Vue application instance. You may then begin
 * registering components with the application instance so they are ready
 * to use in your application's views. An example is included for you.
 */

import { createApp } from "vue";
import App from "./app/components/layouts/App.vue";
import Auth from "./app/components/layouts/Auth.vue";
import Main from "./app/components/layouts/Main.vue";
import Navbar from "./app/components/layouts/Navbar.vue";
import TransactionCard from "./app/components/reuseable-components/TransactionCard.vue";
import router from "./app/route";
import store from "./app/store";

import { VueGoodTable } from "vue-good-table";
import "vue-good-table/dist/vue-good-table.css";

const app = createApp(App);

app.use(router);
app.use(store);
app.use(VueGoodTable);

app.component("main-layout", Main);
app.component("app-layout", App);
app.component("auth-layout", Auth);
app.component("app-navbar", Navbar);

// Reuseable components
app.component("transaction-card", TransactionCard);

app.mount("#app");
