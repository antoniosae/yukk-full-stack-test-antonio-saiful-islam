import axios from "axios";

const postAction = (url, data) => {
    axios.post(url, data);
};

export { postAction };
