import { createStore } from "vuex";

import axios from "axios";

export default new createStore({
    state: {
        status: "",
        token: localStorage.getItem("accessToken") || "",
        user: localStorage.getItem("user") || {},
    },

    mutations: {
        auth_request(state) {
            state.status = "loading";
        },
        auth_success(state, { token, user }) {
            state.status = 1;
            state.token = token;
            state.user = user;
        },
        auth_error(state) {
            state.status = "error";
        },
        logout(state) {
            state.status = "";
            state.token = "";
            localStorage.removeItem("user");
            state.user = {};
        },
    },

    actions: {
        login({ commit }, user) {
            return new Promise((resolve, reject) => {
                commit("auth_request");
                axios
                    .post("/api/auth/login", user)
                    .then((res) => {
                        let token = res.data.response.token;
                        let user = res.data.response.user;

                        localStorage.setItem("accessToken", token);
                        localStorage.setItem("user", JSON.stringify(user));

                        axios.defaults.headers.common["Authorization"] = token;

                        commit("auth_success", {
                            token,
                            user,
                        });

                        resolve(res);
                    })
                    .catch((err) => {
                        commit("auth_error");
                        resolve(res);
                        localStorage.removeItem("accessToken");
                        reject(err);
                    });
            });
        },

        logout({ commit }) {
            return new Promise((resolve, reject) => {
                commit("logout");

                axios.post("/api/auth/logout");
                localStorage.removeItem("accessToken");
                localStorage.removeItem("user");
                delete axios.defaults.headers.common["Authorization"];
                resolve();
            });
        },
    },

    getters: {
        isLoggedIn: (state) => !!state.token,
        authStatus: (state) => state.status,
        user: (state) => state.user,
    },
});
