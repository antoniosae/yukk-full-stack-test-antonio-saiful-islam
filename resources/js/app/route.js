//import vue router
import axios from "axios";
import { createRouter, createWebHistory } from "vue-router";
import store from "./store";

// Components Import
import TransactionIndex from "./pages/transactions/Index.vue";
import TransactionSend from "./pages/transactions/Send.vue";
import TransactionTopUp from "./pages/transactions/TopUp.vue";
//define a routes
const routes = [
    {
        path: "/",
        name: "home",
        meta: {
            requiresAuth: true,
        },
        component: () =>
            import(/* webpackChunkName: "home" */ "./pages/profile/Index.vue"),
    },
    {
        path: "/register",
        name: "auth.register",
        component: () =>
            import(/* webpackChunkName: "index" */ "./pages/auth/Register.vue"),
    },
    {
        path: "/login",
        name: "auth.login",
        component: () =>
            import(/* webpackChunkName: "create" */ "./pages/auth/Login.vue"),
    },
    {
        path: "/transaction",
        meta: {
            requiresAuth: true,
        },
        children: [
            {
                path: "",
                component: TransactionIndex,
            },
            {
                path: "top-up",
                component: TransactionTopUp,
            },
            {
                path: "send",
                component: TransactionSend,
            },
        ],
    },
];

//create router
const router = createRouter({
    history: createWebHistory(),
    routes, // <-- routes,
});

let accessTokenLocalStorage = localStorage.getItem("accessToken");

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (accessTokenLocalStorage) {
            // Handle 401 error
            axios.interceptors.response.use(
                (response) => {
                    return response;
                },
                (error) => {
                    if (error.response.status === 401) {
                        axios.post("/api/auth/logout");
                        localStorage.removeItem("accessToken");
                        localStorage.removeItem("user");
                        delete axios.defaults.headers.common["Authorization"];
                        window.location = "/";
                    }
                    return error;
                }
            );
            // ENd of handle 401 error
            axios.interceptors.request.use(
                function (config) {
                    const token = store.state.token;
                    if (token) config.headers.Authorization = `Bearer ${token}`;
                    return config;
                },
                function (error) {
                    return Promise.reject(error);
                }
            );
            if (!accessTokenLocalStorage) {
                window.location.href = "/login";
            }

            next();
            return;
        }
        next("/login");
    } else {
        next();
    }
});

router.beforeResolve((to, from, next) => {
    if (to.path) {
        // window.confirm('Do you really want to leave? you have unsaved changes!')
        // if (window.confirm("Do you really want to leave?")) {
        // }
    }
    next();
});

router.afterEach((to, from, next) => {
    // pace.done()
});

export default router;
