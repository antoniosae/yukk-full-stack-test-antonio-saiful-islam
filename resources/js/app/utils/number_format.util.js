const numberFormat = (number) => {
    const formatter = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR", // Use the appropriate currency code for your locale
        minimumFractionDigits: 2, // Maintain two decimal places
    });

    const formattedNumber = formatter.format(number);

    return formattedNumber;
};

export { numberFormat };
