# Use the official PHP image with the specified version
FROM php:8.3.2

# Set the working directory in the container
WORKDIR /var/www/html

# Install system dependencies
RUN apt-get update && \
    apt-get install -y \
    git \
    unzip \
    libzip-dev \
    libpq-dev \
    && docker-php-ext-install zip pdo pdo_pgsql

# Install Composer globally
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Node.js and npm
RUN curl -sL https://deb.nodesource.com/setup_20.x | bash -
RUN apt-get install -y nodejs

# Install PostgreSQL client
RUN apt-get install -y postgresql-client

# Install Laravel
RUN composer create-project --prefer-dist laravel/laravel .

# Copy the existing application directory contents
COPY . /var/www/html

# Install PHP extensions and configure
RUN chown -R www-data:www-data /var/www/html
RUN chmod -R 775 storage bootstrap/cache

# Install npm dependencies and build assets
RUN npm install
RUN npm run prod

# Expose port 80 for the web server
EXPOSE 80

# Start PHP built-in server
CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=80"]
