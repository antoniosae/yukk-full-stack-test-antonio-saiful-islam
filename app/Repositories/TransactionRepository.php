<?php

namespace App\Repositories;

use App\Models\Transaction;
use App\Repositories\Interfaces\TransactionRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class TransactionRepository implements TransactionRepositoryInterface {
    public function getAllTransactions(array $requestData): LengthAwarePaginator {
        $transactionPagination = Transaction::query();

        $transactionPagination->when(isset($requestData["searchTerm"]), function ($query) use ($requestData) {
            $query->where("description", "ILIKE", "%".$requestData['searchTerm']. "%")->orWhere("trx_code", "ILIKE", "%".$requestData['searchTerm']. "%");
        });
        $transactionPagination->when(isset($requestData["userId"]), function ($query) use ($requestData) {
            $query->where("user_id", $requestData['userId']);
        });
        $transactionPagination->when(isset($requestData["type"]), function ($query) use ($requestData) {
            $query->where("type", $requestData['type']);
        });

        $transactionPagination->orderBy('id', 'DESC');

        return $transactionPagination->paginate(isset($requestData['perPage']) ? $requestData['perPage'] : 10);
    }

    public function addDebitIntoTransaction(int $userId, float $amount, string $description, string $evidence_file): void {
        Transaction::create([
            'user_id' => $userId,
            'trx_code' => $this->getNextTransactionUniqueCode(),
            'type' => "debit",
            'description' => $description,
            'evidence_file' => $evidence_file,
            'amount' => $amount
        ]);
    }

    public function addCreditIntoTransaction(int $userId, float $amount, string $description, string $evidence_file): void {
        Transaction::create([
            'user_id' => $userId,
            'trx_code' => $this->getNextTransactionUniqueCode(),
            'type' => "credit",
            'description' => $description,
            'evidence_file' => $evidence_file,
            'amount' => $amount
        ]);
    }

    public function getNextTransactionUniqueCode(): string {

        //TODO: Note we can modify this pattern depends on company policy
        $transactionCount = Transaction::count() + 1;
        return "TRX-" . rand(00,99) . str_pad($transactionCount, 12, '0', STR_PAD_LEFT);
    }
}
