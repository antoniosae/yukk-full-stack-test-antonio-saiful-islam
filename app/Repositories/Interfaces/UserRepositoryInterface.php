<?php

namespace App\Repositories\Interfaces;

use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface UserRepositoryInterface {
    public function getAllUsers(array $requestData): LengthAwarePaginator;
    public function getSingleUserById(int $userId): User;
    public function getSingleUserByEmail(string $userId): User;
    public function changePassword(int $userId, string $password): void;
    public function deleteUserById(int $userId): void;
    public function updateUserById(int $userId, array $newUserData): User;
    public function createNewUser(array $newUserData): User;
}
