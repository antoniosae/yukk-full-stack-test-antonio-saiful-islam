<?php

namespace App\Repositories\Interfaces;

use App\Models\Balance;

interface BalanceRepositoryInterface {

    public function createInitializationBalance(int $userId): Balance;
    public function getCurrentBalanceByUserId(int $userId): Balance;
    public function addBalance(int $userId, float $amount): void;
    public function reduceBalance(int $userId, float $amount): void;
}
