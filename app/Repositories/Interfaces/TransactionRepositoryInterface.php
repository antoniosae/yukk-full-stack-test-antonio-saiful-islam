<?php

namespace App\Repositories\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
interface TransactionRepositoryInterface {
    public function getAllTransactions(array $transactionFilter): LengthAwarePaginator;
    public function addDebitIntoTransaction(int $userId, float $amount, string $description, string $evidence_file): void;
    public function addCreditIntoTransaction(int $userId, float $amount, string $description, string $evidence_file): void;
    public function getNextTransactionUniqueCode(): string;
}
