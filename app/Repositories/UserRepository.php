<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface {

    public function getAllUsers(array $requestData): LengthAwarePaginator {
        $userPagination = User::query();
        $userPagination->when(isset($requestData["searchTerm"]), function ($query) use ($requestData) {
            $query->where("name", "ILIKE", "%".$requestData['searchTerm']. "%");
        });

        return $userPagination->paginate(isset($requestData['perPage']) ? $requestData['perPage'] : 10);
    }

    public function getSingleUserById(int $userId): User {
        return User::findOrFail($userId);
    }

    public function getSingleUserByEmail(string $email): User {
        return User::where('email', $email)->first();
    }

    public function changePassword(int $userId, string $password): void {
        $user = $this->getSingleUserById($userId);
        $user->update(['password' => Hash::make($password)]);
    }

    public function deleteUserById(int $userId): void {
        User::findOrFail($userId)->delete();
    }

    public function updateUserById(int $userId, array $newUserData): User {
        $user = User::findOrFail($userId);
        $user->update($newUserData);

        return $user;
    }

    public function createNewUser(array $newUserData): User {
        return User::create($newUserData);
    }
}
