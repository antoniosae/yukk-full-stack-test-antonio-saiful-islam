<?php

namespace App\Repositories;

use App\Exceptions\BalanceException;
use App\Models\Balance;
use App\Repositories\Interfaces\BalanceRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BalanceRepository implements BalanceRepositoryInterface {

    public function createInitializationBalance(int $userId): Balance {
        return Balance::create([
            'user_id' => $userId,
            'amount' => 0.00,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    public function getCurrentBalanceByUserId(int $userId): Balance {
        $balance = Balance::where('user_id', $userId)->first();

        if($balance == null) {
            $balance = $this->createInitializationBalance($userId);
        }

        return $balance;
    }

    public function addBalance(int $userId, float $amount): void {
        $balance = $this->getCurrentBalanceByUserId($userId);
        $balance->amount = $balance->amount + $amount;
        $balance->save();
    }
    public function reduceBalance(int $userId, float $amount): void {
        $balance = $this->getCurrentBalanceByUserId($userId);
        $balance->amount = $balance->amount - $amount;
        $balance->save();
    }
}
