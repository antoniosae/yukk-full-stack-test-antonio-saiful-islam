<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'trx_transactions';

    protected $fillable = [
        'user_id',
        'trx_code',
        'type',
        'description',
        'evidence_file',
        'amount'
    ];
}
