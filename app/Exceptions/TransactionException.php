<?php

namespace App\Exceptions;

use App\Utils\HttpResponseUtil;

use Exception;

class TransactionException extends Exception {

    public function render()
    {
        return HttpResponseUtil::error(null, "Can't process your Transaction",422);
    }
}
