<?php

namespace App\Exceptions;

use App\Utils\HttpResponseUtil;

use Exception;

class BalanceException extends Exception {

    public function render()
    {
        return HttpResponseUtil::error(null, "Can't process your request",422);
    }
}
