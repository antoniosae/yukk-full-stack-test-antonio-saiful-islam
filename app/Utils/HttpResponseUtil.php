<?php

namespace App\Utils;

class HttpResponseUtil
{
    public static function success($response, $message = null, $status = 200)
    {
        return response()->json([
            'message' => $message ?? "Successfully",
            'response' => $response
        ], $status);
    }

    public static function error($response, $message = null, $status = 500)
    {
        return response()->json([
            'message' => $message,
            'response' => $response,
        ], $status);
    }
}
