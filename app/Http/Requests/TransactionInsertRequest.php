<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;


class TransactionInsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'numeric|min:1',
            'destination_user_id' => 'numeric|min:1',
            'trx_code' => 'regex:/^[\w-]*$/',
            'type' => 'in:debit,credit',
            'amount' => 'numeric|min:5000|required',
            'evidence_file' => 'file|required',
            'description' => 'regex:/^[\w-]*$/'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $response = $validator->errors();

        throw new HttpResponseException(response()->json($response, 400));
    }
}
