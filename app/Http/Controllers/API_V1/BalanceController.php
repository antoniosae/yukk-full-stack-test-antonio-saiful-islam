<?php

namespace App\Http\Controllers\API_V1;

use App\Http\Controllers\Controller;
use App\Services\BalanceService;
use App\Utils\HttpResponseUtil;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    private $balanceService;

    public function __construct(BalanceService $balanceService) {
        $this->balanceService = $balanceService;
    }


    public function index(Request $request) {
        $id = auth()->user()->id;

        return HttpResponseUtil::success(
            $this->balanceService->getCurrentBalanceByUserId($id),
            "Successfully fetched your Balance",
            200
        );
    }
}
