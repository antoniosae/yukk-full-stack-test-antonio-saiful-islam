<?php

namespace App\Http\Controllers\API_V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\TransactionInsertRequest;
use App\Http\Requests\TransactionSearchRequest;
use App\Http\Requests\TransactionSendMoneyRequest;
use App\Services\TransactionService;
use App\Utils\HttpResponseUtil;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $transactionService;

    public function __construct(TransactionService $transactionService) {
        $this->transactionService = $transactionService;
    }

    public function index(TransactionSearchRequest $request) {
        $request['userId'] = auth()->user()->id;
        return HttpResponseUtil::success(
            $this->transactionService->getAllTransactions($request->all()),
            "Successfully fetched Transaction by User",
            200
        );
    }

    public function topUp(TransactionInsertRequest $request) {

        $description = "Top Up sebesar Rp. " . number_format($request->amount);

        $fileName = $this->processFileWithReturnFileName($request->evidence_file);

        $this->transactionService->addDebitIntoTransaction(auth()->user()->id, $request->amount, $description, $fileName);

        return HttpResponseUtil::success(
            null,
            "Top Up Successfully",
            201
        );
    }

    public function addCredit(TransactionInsertRequest $request) {

        $description = "Kredit sebesar Rp. " . number_format($request->amount);

        $fileName = $this->processFileWithReturnFileName($request->evidence_file);

        $this->transactionService->addCreditIntoTransaction(auth()->user()->id, $request->amount, $description, $fileName);

        return HttpResponseUtil::success(
            null,
            "Successfully fetched Transaction by User",
            201
        );
    }

    public function sendMoney(TransactionSendMoneyRequest $request) {

        $description = "Debit sebesar Rp. " . number_format($request->amount);

        $fileName = $this->processFileWithReturnFileName($request->evidence_file);

        error_log("userId " . auth()->user()->id ."". $request->amount ."". $description);

        $this->transactionService->sendMoney(auth()->user()->id, $request->destination_user_id, $request->amount, $fileName);

        return HttpResponseUtil::success(
            null,
            "Send Money is Success",
            201
        );
    }

    public function addDebit(TransactionInsertRequest $request) {

        $description = "Debit sebesar Rp. " . number_format($request->amount);

        $fileName = $this->processFileWithReturnFileName($request->evidence_file);

        $this->transactionService->addDebitIntoTransaction(auth()->user()->id, $request->amount, $description, $fileName);

        return HttpResponseUtil::success(
            null,
            "Successfully fetched Transaction by User",
            201
        );
    }

    private function processFileWithReturnFileName($file): string {
        $fileName = time() . '_' . $file->getClientOriginalName();
        $filePath = $file->storeAs('uploads', $fileName, 'public');

        return "storage/".$filePath;
    }
}
