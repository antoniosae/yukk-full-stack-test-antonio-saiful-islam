<?php

namespace App\Http\Controllers\API_V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserInsertRequest;
use App\Http\Requests\UserSearchRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;
use App\Utils\HttpResponseUtil;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(UserSearchRequest $request)
    {
        return HttpResponseUtil::success($this->userService->getAllUsers($request->all()), "Successfully fetched pagination data of Users");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserInsertRequest $request)
    {
        return HttpResponseUtil::success($this->userService->createNewUser($request->all()), "Successfully created new User", 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return HttpResponseUtil::success($this->userService->getSingleUserById($id), "Successfully fetched user by ID $id");
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserUpdateRequest $request, string $id)
    {
        return HttpResponseUtil::success($this->userService->updateUserById($id, $request->all()), "Successfully updated user by ID $id");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->userService->deleteUserById($id);
        return HttpResponseUtil::success(null, "Successfully deleted User", 200);
    }
}
