<?php

namespace App\Http\Controllers\API_V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\UserProfileUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;
use App\Utils\HttpResponseUtil;

class ProfileController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function index() {
        $user = $this->userService->getSingleUserById(auth()->user()->id);

        return HttpResponseUtil::success($user, "Successfully fetched user Profile", 200);
    }

    public function updateProfile(UserProfileUpdateRequest $request) {
        $id = auth()->user()->id;

        return HttpResponseUtil::success($this->userService->updateUserById($id, $request->all()), "Successfully updated your profile");
    }

    public function changePassword(PasswordRequest $request) {

        $id = auth()->user()->id;

        $this->userService->changePassword($id, $request->password);

        return HttpResponseUtil::success(
            null,
            "Password successfully changed",
            200
        );
    }
}
