<?php

namespace App\Http\Controllers\API_V1;

use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;
use App\Utils\HttpResponseUtil;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\UserInsertRequest;
use Hash;

class AuthController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }
    public function register(UserInsertRequest $request)
    {
        $user = $this->userService->createNewUser($request->all());

        $token = $user->createToken('myapptoken')->plainTextToken;

        return HttpResponseUtil::success(
            [
                "user" => $user,
                "token" => $token
            ],
            "Successfully fetched pagination data of Users",
            201
        );
    }

    public function getProfile()
    {
        $user = auth()->user();
        return HttpResponseUtil::success($user, "Successfully fetched User", 200);
    }

    public function login(LoginRequest $request)
    {
        $user = $this->userService->getSingleUserByEmail($request->email);

        if (!Hash::check($request->password, $user->password)) {
            return HttpResponseUtil::error(['password' => ['Wrong password']], "Can't process your request", 401);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;

        return HttpResponseUtil::success(
            [
                "user" => $user,
                "token" => $token
            ],
            "Successfully fetched pagination data of Users",
            200
        );
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return HttpResponseUtil::success(
            null,
            "Successfully log out",
            200
        );
    }

    public function updateProfile(UserUpdateRequest $request)
    {
        $id = auth()->user()->id;
        return HttpResponseUtil::success($this->userService->updateUserById($id, $request->all()), "Successfully updated your Profile");
    }

    public function changePassword(PasswordRequest $request)
    {
        $id = auth()->user()->id;

        $this->userService->changePassword($id, $request->password);

        return HttpResponseUtil::success(
            null,
            "Password successfully changed",
            200
        );
    }
}
