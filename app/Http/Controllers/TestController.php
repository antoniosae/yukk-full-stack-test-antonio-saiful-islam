<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TestController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function test(Request $request) {
        // return $this->userService->getAllUsers($request->all());
        // return $this->userService->getSingleUserById(5);
        $user = [
            'name' => "Test New User",
            'email' => "updateNewUs42@email.com",
            'password' => Hash::make('password'), // Hash the password
        ];
        return $this->userService->updateUserById(22, $user);
    }
}
