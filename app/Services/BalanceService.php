<?php

namespace App\Services;
use App\Exceptions\BalanceException;
use App\Models\Balance;
use App\Repositories\BalanceRepository;
use App\Services\Interfaces\BalanceServiceInterface;
use Illuminate\Support\Facades\DB;

class BalanceService implements BalanceServiceInterface {

    private $balanceRepository;

    public function __construct(BalanceRepository $balanceRepository) {
        $this->balanceRepository = $balanceRepository;
    }

    public function getCurrentBalanceByUserId(int $userId): Balance {
        return $this->balanceRepository->getCurrentBalanceByUserId($userId);
    }

    public function addBalance(int $userId, float $amount): void {
        DB::beginTransaction();

        try {
            $this->balanceRepository->addBalance($userId, $amount);

            DB::commit();

        } catch(BalanceException $e) {
            DB::rollBack();
            throw $e;
        }
    }
    public function reduceBalance(int $userId, float $amount): void {
        DB::beginTransaction();

        try {

            $balance = $this->getCurrentBalanceByUserId($userId);

            if($balance->amount - $amount <= 0) {
                DB::rollBack();

                throw new BalanceException();
            }

            $this->balanceRepository->reduceBalance($userId, $amount);

            DB::commit();

        } catch(BalanceException $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
