<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\BalanceRepository;
use App\Repositories\UserRepository;
use App\Services\Interfaces\UserServiceInterface;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class UserService implements UserServiceInterface {

    protected $userRepository;
    protected $balanceRepository;
    public function __construct(UserRepository $userRepository, BalanceRepository $balanceRepository) {
        $this->userRepository = $userRepository;
        $this->balanceRepository = $balanceRepository;
    }

    public function getAllUsers(array $requestData): LengthAwarePaginator {
        return $this->userRepository->getAllUsers($requestData);
    }

    public function getSingleUserById(int $userId): User {
        return $this->userRepository->getSingleUserById($userId);
    }
    public function getSingleUserByEmail(string $email): User {
        return $this->userRepository->getSingleUserByEmail($email);
    }
    public function changePassword(int $userId, string $newPassword): void {
        $this->userRepository->changePassword($userId, $newPassword);
    }
    public function deleteUserById(int $userId): void {
        $this->userRepository->deleteUserById($userId);
    }
    public function updateUserById(int $userId, array $newUserData): User {
        return $this->userRepository->updateUserById($userId, $newUserData);
    }
    public function createNewUser(array $newUserData): User {
        DB::beginTransaction();
        try {
            $user = $this->userRepository->createNewUser($newUserData);

            $this->balanceRepository->createInitializationBalance($user->id);

            DB::commit();

            return $user;
        } catch(Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
