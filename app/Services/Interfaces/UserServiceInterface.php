<?php

namespace App\Services\Interfaces;

use App\Models\User;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface UserServiceInterface {
    public function getAllUsers(array $requestData): LengthAwarePaginator;
    public function getSingleUserById(int $userId): User;
    public function getSingleUserByEmail(string $email): User;
    public function changePassword(int $userId, string $newPassword): void;
    public function deleteUserById(int $userId): void;
    public function updateUserById(int $userId, array $newUser): User;
    public function createNewUser(array $newUserData): User;
}
