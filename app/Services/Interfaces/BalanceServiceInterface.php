<?php

namespace App\Services\Interfaces;

use App\Models\Balance;

interface BalanceServiceInterface {
    public function getCurrentBalanceByUserId(int $userId): Balance;
    public function addBalance(int $userId, float $amount): void;
    public function reduceBalance(int $userId, float $amount): void;
}
