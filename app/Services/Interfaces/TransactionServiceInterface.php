<?php

namespace App\Services\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface TransactionServiceInterface {
    public function getAllTransactions(array $transactionFilter): LengthAwarePaginator;
    public function addDebitIntoTransaction(int $userId, float $amount, string $description, string $evidence_file): void;
    public function addCreditIntoTransaction(int $userId, float $amount, string $description, string $evidence_file): void;
    public function sendMoney(int $userId, int $destinationUserId, float $amount, string $evidence_file): void;
}
