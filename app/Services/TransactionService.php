<?php

namespace App\Services;

use App\Exceptions\TransactionException;
use App\Repositories\TransactionRepository;
use App\Services\Interfaces\TransactionServiceInterface;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class TransactionService implements TransactionServiceInterface {

    private $transactionRepository;
    private $balanceService;
    private $userService;

    public function __construct(TransactionRepository $transactionRepository, BalanceService $balanceService, UserService $userService) {
        $this->transactionRepository = $transactionRepository;
        $this->balanceService = $balanceService;
        $this->userService = $userService;
    }

    public function getAllTransactions(array $transactionFilter): LengthAwarePaginator {
        error_log("transactionFilter => " . $transactionFilter['userId']);
        return $this->transactionRepository->getAllTransactions($transactionFilter);
    }
    public function addDebitIntoTransaction(int $userId, float $amount, string $description, string $evidence_file): void {
        DB::beginTransaction();

        try {
            $this->balanceService->addBalance($userId, $amount);
            $this->transactionRepository->addDebitIntoTransaction($userId, $amount, $description, $evidence_file);

            DB::commit();
        } catch(TransactionException $e) {
            DB::rollback();
            throw $e;
        }
    }
    public function addCreditIntoTransaction(int $userId, float $amount, string $description, string $evidence_file): void {
        DB::beginTransaction();

        try {

            $balance = $this->balanceService->getCurrentBalanceByUserId($userId);

            if($balance->amount - $amount <= 0) {
                DB::rollBack();
                throw new TransactionException();
            }

            $this->transactionRepository->addCreditIntoTransaction($userId, $amount, $description, $evidence_file);
            $this->balanceService->reduceBalance($userId, $amount);

            DB::commit();
        } catch(TransactionException $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function sendMoney(int $userId, int $destinationUserId, float $amount, string $evidence_file): void {
        DB::beginTransaction();

        try {

            $sender = $this->userService->getSingleUserById($userId);
            $receiver = $this->userService->getSingleUserById($destinationUserId);

            $descriptionSender = "Transfer sebesar Rp. " . number_format($amount,2,".","") . " ke " . $receiver->name;
            $descriptionReceiver = "Menerima Transfer sebesar Rp. " . number_format($amount,2,".","") . " dari " . $sender->name;

            $balance = $this->balanceService->getCurrentBalanceByUserId($userId);

            if($balance->amount - $amount <= 0) {
                DB::rollBack();
                throw new TransactionException();
            }

            $this->transactionRepository->addCreditIntoTransaction($userId, $amount, $descriptionSender, $evidence_file);
            $this->transactionRepository->addDebitIntoTransaction($destinationUserId, $amount, $descriptionReceiver, $evidence_file);
            $this->balanceService->reduceBalance($userId, $amount);

            DB::commit();
        } catch(TransactionException $e) {
            DB::rollback();
            throw $e;
        }
    }
}
