<?php

use App\Http\Controllers\API_V1\AuthController as AuthControllerV1;
use App\Http\Controllers\API_V1\BalanceController as BalanceControllerV1;
use App\Http\Controllers\API_V1\ProfileController as ProfileControllerV1;
use App\Http\Controllers\API_V1\UserController as UserControllerV1;
use App\Http\Controllers\API_V1\TransactionController as TransactionControllerV1;
use App\Http\Controllers\TestController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::resource('users', UserControllerV1::class);

    Route::group(['prefix' => 'profile'], function() {
        Route::get('/', [ProfileControllerV1::class,'index']);
        Route::put('/change-password', [ProfileControllerV1::class,'changePassword']);
        Route::put('/update-profile', [ProfileControllerV1::class,'updateProfile']);
    });

    // Balance
    Route::get('/balance', [BalanceControllerV1::class,'index']);

    Route::group(['prefix' => 'transaction'], function() {
        Route::get('/', [TransactionControllerV1::class,'index']);
        Route::post('/debit', [TransactionControllerV1::class,'addDebit']);
        Route::post('/credit', [TransactionControllerV1::class,'addCredit']);
        Route::post('/send-money', [TransactionControllerV1::class,'sendMoney']);
        Route::post('/top-up', [TransactionControllerV1::class,'topUp']);
    });
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthControllerV1::class, 'login'])->name('login');
    Route::post('/register', [AuthControllerV1::class, 'register']);
});
