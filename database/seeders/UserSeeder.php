<?php

namespace Database\Seeders;

use App\Services\UserService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash; // For password hashing
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    protected $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function run()
    {
        $faker = Faker::create();

        // Static Users
        $this->userService->createNewUser([
            'name' => "user1",
            'email' => "user1@mail.com",
            'password' => Hash::make('password'),
        ]);
        $this->userService->createNewUser([
            'name' => "user2",
            'email' => "user2@mail.com",
            'password' => Hash::make('password'),
        ]);
        $this->userService->createNewUser([
            'name' => "user3",
            'email' => "user3@mail.com",
            'password' => Hash::make('password'),
        ]);

        foreach (range(1, 50) as $index) { // Create 50 users
            $this->userService->createNewUser([
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'password' => Hash::make('password'),
            ]);
        }
    }
}
