<?php

namespace Database\Seeders;

use App\Services\TransactionService;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    protected $transactionService;

    public function __construct(TransactionService $transactionService) {
        $this->transactionService = $transactionService;
    }
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Initialization balance
        $this->topUpInitialization(1, 1000000);
        $this->topUpInitialization(2, 1000000);
        $this->topUpInitialization(3, 1000000);

        $this->sendMoneySeederInitialization(1, 2, 12000);
        $this->sendMoneySeederInitialization(1, 3, 45000);
        $this->sendMoneySeederInitialization(1, 3, 56000);
        $this->sendMoneySeederInitialization(1, 3, 21000);
        $this->sendMoneySeederInitialization(1, 3, 9000);
        $this->sendMoneySeederInitialization(2, 1, 50000);
    }

    private function topUpInitialization($user_id, $amount): void {
        $description = "Top Up sebesar Rp. " . number_format($amount, 2);
        $fileName = "https://i.pinimg.com/736x/2c/b6/e7/2cb6e71ba6fd63d745708bda2bb48022.jpg";

        $this->transactionService->addDebitIntoTransaction($user_id, $amount, $description, $fileName);
    }

    private function sendMoneySeederInitialization($sender_user_id, $receiver_user_id, $amount): void {
        $fileName = "https://i.pinimg.com/736x/2c/b6/e7/2cb6e71ba6fd63d745708bda2bb48022.jpg";

        $this->transactionService->sendMoney($sender_user_id, $receiver_user_id, $amount, $fileName);
    }
}
