<?php

namespace Tests\Feature;

use App\Models\User;
use App\Services\Interfaces\UserServiceInterface;
use App\Services\UserService;
use Tests\TestCase;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;


class UserTest extends TestCase
{

    protected $userService;

    public function __construct(UserService $userService) {
        parent::setUp();
        $this->userService = $userService;
    }

    public function test_getAllUsers_returns_paginator()
    {
        $paginator = $this->userService->getAllUsers([]);

        $this->assertInstanceOf(LengthAwarePaginator::class, $paginator);
    }
}
