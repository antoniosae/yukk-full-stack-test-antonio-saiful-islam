# Yukk Full Stack Test
This project was built with:

 1. Laravel 10.43.0
 2. PHP 8.3.2
 3. NodeJS 20.11.0
 4. Composer 2.6.6
 5. PostgreSQL 15.3 (or you can use popular RDBMS such as MySQL, SQLite. Just change the driver in .env)
## How to Run Project
### Run project manually
1. Clone this repository
2. Run `composer install`
3. Copy .env.example to .env `cp .env.example .env`
4. Run `php artisan key:generate`
5. Run `chown  -R  www-data:www-data  storage  bootstrap/cache` 
6. Run `chmod  -R  775  storage  bootstrap/cache` to add permision
7. Config your database configuration inside .env file

`nano .env` or `vi .env` or `vim .env`

    DB_CONNECTION=pgsql #select one:  sqlite, mysql, pgsql, sqlsrv
    DB_HOST=127.0.0.1 # enter the correct host
    DB_PORT=5435 # enter db port
    DB_DATABASE=payment_gateway # enter db name
    DB_USERNAME=payment_gw_user # enter db username
    DB_PASSWORD=mystr0ngp4sswr0d # enter db pasword

9. Run `php artisan migrate:fresh --seed`
10. Run `npm install` and `npm run prod` or `npm run dev`
11. Last, run `php artisan serve`

### Run within Docker
Unfortunately, this section is not available. However, i created docker-compose.yaml that contains postgre:15.3 image to generate container. So you will be to access the postgre database easily.

`docker-compose up -d --force-recreate --remove-orphans`

 